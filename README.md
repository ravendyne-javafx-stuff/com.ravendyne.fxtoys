## FXToys

Few odd bits and pieces to ease JavaFX based app development

- Draggable component helper
- Bezier curve Pane
- Simple connected graph Pane with graph animation
- Backgroud command runner for work intensive tasks
- Byte array `PrintWriter` helper
- etc.

### Dependencies

[Constellation library](https://gitlab.com/ravendyne-javafx-stuff/com.ravendyne.constellation)
