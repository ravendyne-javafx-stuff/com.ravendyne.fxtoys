package com.ravendyne.fxtoys.utils;

import java.io.BufferedReader;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.PrintWriter;

public class LibUtil {

    private LibUtil() {}

    public static byte[] readBytes(InputStream inStream) throws IOException {
        ByteArrayOutputStream buffer = new ByteArrayOutputStream();

        int nRead;
        byte[] data = new byte[16384];

        while ((nRead = inStream.read(data, 0, data.length)) != -1) {
          buffer.write(data, 0, nRead);
        }

        buffer.flush();

        return buffer.toByteArray();        
    }
    
    public static String readLines(InputStream inputStream) throws IOException {
        BufferedReader stdInput = new BufferedReader(new InputStreamReader(inputStream));
        final ByteArrayOutputStream baos = new ByteArrayOutputStream();
        PrintWriter writer = new PrintWriter(baos);

        // read the output from the command
        String s = null;
        while ((s = stdInput.readLine()) != null) {
            writer.println(s);
        }
        writer.flush();
        
        return baos.toString();
    }

}
