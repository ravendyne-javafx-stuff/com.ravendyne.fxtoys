package com.ravendyne.fxtoys.utils;

import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.nio.file.Path;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.Future;
import java.util.function.Consumer;

import javax.swing.SwingWorker;

public class CommandRunner {
    
    private CommandRunner() {
    }

	public static class CommandResult {
	    /**
	     * Value returned by OS after the command has finished execution.
	     */
		public int exitValue;
		/**
		 * Collected stdout of the command execution, if any.
		 */
		public byte[] processOutputContent;
		/**
		 * Collected stderr of the command execution, if any.
		 */
		public byte[] processErrorContent;
		
		/**
		 * Shallow copy of values.
		 * 
		 * @param other
		 */
		public void copyFrom(CommandResult other) {
            this.exitValue = other.exitValue;
            this.processOutputContent = other.processOutputContent;
            this.processErrorContent = other.processErrorContent;
		}
	}
	
	/**
	 * Runs OS command as external process and returns the execution result.
	 * <br>
	 * <br>
	 * This implementation assumes that the command will receive its input via stdin and will return the result via stdout.
	 * <br>
	 * {@code stdout} is not parsed or interpreted in any way, it is simply collected into {@link CommandResult#processOutputContent}.
	 * <br>
	 * Result of execution is returned in a {@link CommandResult} object.
	 * 
	 */
	public static class Worker {
	    private final String[] command;
	    private final Path workingDirecotry;
	    private final String standardInputForCommand;
	    private final Consumer<CommandResult> consumer;

	    private CommandResult result;

        public Worker(String[] command, Path workingDirecotry, String standardInputForCommand) {
            this.command = command;
            this.workingDirecotry= workingDirecotry;
            this.standardInputForCommand = standardInputForCommand;
            this.consumer = null;
        }

        public Worker(String[] command, Path workingDirecotry, String standardInputForCommand, Consumer<CommandResult> consumer) {
            this.command = command;
            this.workingDirecotry= workingDirecotry;
            this.standardInputForCommand = standardInputForCommand;
            this.consumer = consumer;
        }

        protected CommandResult executeCommand() throws IOException {
            ProcessBuilder pb = new ProcessBuilder(command);

//          Map<String, String> env = pb.environment();
            // If you want clean environment, call env.clear() first
            // env.clear()

//          env.put("VAR1", "myValue");
//          env.remove("OTHERVAR");
//          env.put("VAR2", env.get("VAR1") + "suffix");

            
            if(workingDirecotry != null) {
                File workingFolder = workingDirecotry.toAbsolutePath().toFile();
                pb.directory(workingFolder);
            }

            Process proc = pb.start();
            
            if(standardInputForCommand != null) {
                OutputStream stdOutputStream = proc.getOutputStream();
                stdOutputStream.write(standardInputForCommand.getBytes());
                stdOutputStream.flush();
                stdOutputStream.close();
            }
            
            InputStream stdInputStream = proc.getInputStream();
            InputStream stdErrorStream = proc.getErrorStream();
            byte[] stdInputContent = LibUtil.readBytes(stdInputStream);
            byte[] stdErrorContent = LibUtil.readBytes(stdErrorStream);

            // We must read process' stdin and stderr, which we did above,
            // in order for it to terminate. only then we can call exitValue()
            
            result = new CommandResult();
            try {
                result.exitValue = proc.waitFor();
            } catch ( InterruptedException e ) {
                // FIXME make this an error constant?
                result.exitValue = -2042;
            }
            result.processOutputContent = stdInputContent;
            result.processErrorContent = stdErrorContent;
            
            return result;
        }
        
        protected void done() {
            if(consumer != null) {
                consumer.accept(result);
            }
        }
	}

    public static class GuiWorker extends SwingWorker<CommandResult, Void> {
        private final Worker worker;

        public GuiWorker(String[] command, Path workingDirecotry, String standardInputForCommand) {
            worker = new Worker(command, workingDirecotry, standardInputForCommand);
        }

        public GuiWorker(String[] command, Path workingDirecotry, String standardInputForCommand, Consumer<CommandResult> consumer) {
            worker = new Worker(command, workingDirecotry, standardInputForCommand, consumer);
        }

        @Override
        protected CommandResult doInBackground() throws Exception {
            return worker.executeCommand();
        }
        
        @Override
        protected void done() {
            if(!isCancelled()) {
                worker.done();
            }
        }
    }

    public static class ThreadWorker {
        private final Worker worker;
        private final CommandResult result;
        private final ExecutorService threadExecutor;

        private Future<CommandResult> future;

        public ThreadWorker(String[] command, Path workingDirecotry, String standardInputForCommand) {
            worker = new Worker(command, workingDirecotry, standardInputForCommand);
            result = new CommandResult();
            threadExecutor = Executors.newFixedThreadPool(2);
        }

        public ThreadWorker(String[] command, Path workingDirecotry, String standardInputForCommand, Consumer<CommandResult> consumer) {
            worker = new Worker(command, workingDirecotry, standardInputForCommand, consumer);
            result = new CommandResult();
            threadExecutor = Executors.newFixedThreadPool(2);
        }

        protected void done() {
            if(!future.isCancelled()) {
                worker.done();
            }
        }

        public void execute() {
            // start the processing on one thread
            future = threadExecutor.submit(() -> {
                try {
                    result.copyFrom( worker.executeCommand() );
                } catch (IOException e) {
                    stackTraceToStdError(e);
                }
            }, result);
            
            // wait on the other thread for it to get done
            threadExecutor.submit(() -> {
                try {
                    result.copyFrom( future.get() );
                    done();
                } catch (InterruptedException | ExecutionException e) {
                    stackTraceToStdError(e);
                }
                // shutdown executor when all is done
                threadExecutor.shutdown();
            });
        }

        private void stackTraceToStdError(Exception e) {
            // FIXME use some sensible value
            result.exitValue = -20000;
            StringPrintWriter spw = new StringPrintWriter();
            e.printStackTrace(spw.writer());
            result.processOutputContent = new byte[0];
            result.processErrorContent = spw.asString().getBytes();
        }
    }
    
    /**
     * Runs given command and <b>blocks</b> the current thread execution until the command has completed.
     * 
     * @param command
     * @param workingDirecotry
     * @param standardInputForCommand
     * @return
     * @throws ExecutionException
     */
    public static CommandResult runCommand(String[] command, Path workingDirecotry, String standardInputForCommand) throws ExecutionException {
        CommandResult result = new CommandResult();
        // FIXME make this some sensible value or constant or something...
        result.exitValue = -100000;

        GuiWorker worker = new GuiWorker(command, workingDirecotry, standardInputForCommand);
        worker.execute();
        try {
            result = worker.get();
        } catch (InterruptedException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
        
        return result;
    }
    
    public static void scheduleCommandWithSwing(String[] command, Path workingDirecotry, String standardInputForCommand, Consumer<CommandResult> consumer) throws ExecutionException {
        GuiWorker worker = new GuiWorker(command, workingDirecotry, standardInputForCommand, consumer);
        worker.execute();
    }
    
    public static void scheduleCommandWithThreads(String[] command, Path workingDirecotry, String standardInputForCommand, Consumer<CommandResult> consumer) throws ExecutionException {
        ThreadWorker worker = new ThreadWorker(command, workingDirecotry, standardInputForCommand, consumer);
        worker.execute();
    }

}
