package com.ravendyne.fxtoys.utils;

import java.awt.image.BufferedImage;
import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.nio.file.Paths;
import java.util.concurrent.ExecutionException;
import java.util.function.Consumer;

import javax.imageio.ImageIO;

import com.ravendyne.fxtoys.utils.CommandRunner.CommandResult;

public class DotConverter {
    public static enum InvocationKind {
        SWING,
        THREADS,
        BLOCKING
    }
    
    private final String dotGraphSource;
    private final InvocationKind kind;
    private final Consumer<BufferedImage> consumer;

    private String processOutput;
    private String processError;

    public DotConverter(String dotSource, InvocationKind kind, Consumer<BufferedImage> consumer) {
        this.dotGraphSource = dotSource;
        this.kind = kind;
        this.consumer = consumer;
    }
    
    public void convert() {
        try {

            String theCommand = "";

            if(System.getProperty("os.name").toLowerCase().indexOf("win") >= 0) {
                theCommand = "C:\\Applications\\graphviz-2.38\\bin\\dot.exe";
            }
            if(System.getProperty("os.name").toLowerCase().indexOf("linux") >= 0) {
                theCommand = "dot";
            }
            if(System.getProperty("os.name").toLowerCase().indexOf("os x") >= 0) {
                theCommand = "/usr/local/Cellar/graphviz/2.40.1/bin/dot";
            }
            
            if(InvocationKind.SWING == kind) {

                CommandRunner.scheduleCommandWithSwing(new String[] {theCommand,"-Tpng"}, Paths.get(""), dotGraphSource, (result) -> dothis(result) );

            } else if(InvocationKind.THREADS == kind) {

                CommandRunner.scheduleCommandWithThreads(new String[] {theCommand,"-Tpng"}, Paths.get(""), dotGraphSource, (result) -> dothis(result));

            } else {

                CommandResult result = CommandRunner.runCommand(new String[] {theCommand,"-Tpng"}, Paths.get(""), dotGraphSource );
                dothis(result);

            }

        } catch (ExecutionException exception) {
            // FIXME
            exception.printStackTrace();
        }
        
    }

    private void dothis(CommandResult result) {
        try {
            if(result.exitValue == 0) {
                BufferedImage resultImage = ImageIO.read(new ByteArrayInputStream(result.processOutputContent));
                consumer.accept(resultImage);
            } else {
                processOutput = LibUtil.readLines(new ByteArrayInputStream(result.processOutputContent));
                processError = LibUtil.readLines(new ByteArrayInputStream(result.processErrorContent));

                // TODO don't print to system out but something else?
                if(!processOutput.isEmpty()) {
                    System.out.println("Standard output of the command (if any):");
                    System.out.println();
                    System.out.println(processOutput);
                }

                if(!processError.isEmpty()) {
                    System.out.println("Standard error of the command (if any):");
                    System.out.println();
                    System.out.println(processError);
                }
            }
        } catch (IOException ignored) {
            // IOExceptions come from reads which operate on ByteArrayInputStream, so we should be safe to ignore this catch.
        }
    }
}
