package com.ravendyne.fxtoys.utils;

import java.io.ByteArrayOutputStream;
import java.io.PrintWriter;

public class StringPrintWriter {
    private final PrintWriter writer;
    private final ByteArrayOutputStream baos;
    private boolean closed;
    
    public StringPrintWriter() {
        closed = false;
        baos = new ByteArrayOutputStream();
        writer = new PrintWriter(baos, true);
    }
    
    /**
     * @return print writer to be used or {@code null} if {@link #toString()} has been called already.
     */
    public PrintWriter writer() {
        if(closed) {
            return null;
        }
        
        return writer;
    }
    
    public boolean isClose() {
        return closed;
    }

    public String asString() {
        closed = true;
        writer.flush();
        writer.close();

        return baos.toString();
    }
}
