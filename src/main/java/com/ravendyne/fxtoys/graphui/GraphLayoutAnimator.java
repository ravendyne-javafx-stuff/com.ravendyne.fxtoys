package com.ravendyne.fxtoys.graphui;

import com.ravendyne.constellation.graphui.layout.forcebased.ForceBasedGraphLayout;

import javafx.animation.AnimationTimer;

public class GraphLayoutAnimator {

    final static int TARGET_FPS = 30;
    final static double DELTA   = 0.03; // ~ 1 / 30fps
    private boolean layoutStarted;
    private boolean stopLayout;
    
    private ForceBasedGraphLayout layout;

    public GraphLayoutAnimator(ForceBasedGraphLayout layout) {
        this.layout = layout;

        layoutStarted = false;
        stopLayout = true;
    }
    
    public void stopLayout() {
        stopLayout = true;
    }
    public void startLayout() {

        if (this.layoutStarted) return;
        this.layoutStarted = true;
        this.stopLayout = false;

        AnimationTimer timer = new AnimationTimer() {
            long frameDuration = 0;
            long frameCount = 0;
            long previousFrameTimestamp = 0;
            long cumulativeDeltaTime = 0;
            @Override
            public void handle(long now) {
                long startTime = System.currentTimeMillis();
                // this calculates graph simulation for the next DELTA time interval
                layout.layoutStep(DELTA);
                long deltaTime = System.currentTimeMillis() - startTime;
                cumulativeDeltaTime += deltaTime;
                
                if(previousFrameTimestamp > 0) {
                frameCount ++;
                frameDuration += now - previousFrameTimestamp;
                }
                previousFrameTimestamp = now;

                // stop simulation when energy of the system goes below a threshold
                if (stopLayout || layout.isLayoutDone()) {
                    layoutStarted = false;
                    // this == AnimationTimer
                    this.stop();
                    final long frameTime = frameDuration/frameCount/1000;
                    System.out.println("# of frames: " + frameCount);
                    System.out.println("Layout duration avg: " + cumulativeDeltaTime/frameCount + "mS");
                    System.out.println("Frame duradion avg: " + frameTime / 1000.0 + "mS");
                    System.out.println("FPS avg: " + 1000000L / frameTime + "");
                }
            }
        };
        timer.start();
    }

}
