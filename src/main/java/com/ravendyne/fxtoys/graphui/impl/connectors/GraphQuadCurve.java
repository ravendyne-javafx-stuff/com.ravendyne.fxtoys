package com.ravendyne.fxtoys.graphui.impl.connectors;

import com.ravendyne.constellation.geometry.Vector2d;
import com.ravendyne.constellation.graphui.api.IGraphUIConnector;
import com.ravendyne.constellation.graphui.api.IGraphUINode;

import javafx.scene.Node;
import javafx.scene.shape.QuadCurve;

public class GraphQuadCurve extends QuadCurve implements IGraphUIConnector {
    public static final String CSS_CLASS = "graph-connector-curve";

    protected IGraphUINode startNode;
    protected IGraphUINode endNode;
    
    public GraphQuadCurve( IGraphUINode startNode, IGraphUINode endNode ) {
        getStyleClass().add(CSS_CLASS);

        this.startNode = startNode;
        this.endNode = endNode;
        
        buildUI();
    }

    private void buildUI() {
        setFill(null);
    }

    @Override
    public void setPosition(Vector2d startOnCanvas, Vector2d endOnCanvas) {
        Vector2d P0 = startNode.getPosition();
        Vector2d P2 = endNode.getPosition();

        Vector2d size = P2.subtract(P0);
        double deltaX = size.x / 2.0;
        double deltaY = size.y / 2.0;
        if(Math.abs(deltaX) > Math.abs(deltaY)) {
            deltaY = 0;
        } else {
            deltaX = 0;
        }

        setStartX(P0.x);
        setStartY(P0.y);
        // TODO add left/right, top/bottom switch which alternates P0/P2 in P1 calculation
        setControlX(P0.x + deltaX);
        setControlY(P0.y + deltaY);
        setEndX(P2.x);
        setEndY(P2.y);
    }

    @Override
    public void updatePosition() {
        setPosition(startNode.getPosition(), endNode.getPosition());
    }

    @Override
    public Node getNode() {
        return this;
    }

    @Override
    public IGraphUINode getStartNode() {
        return startNode;
    }

    @Override
    public IGraphUINode getEndNode() {
        return endNode;
    }
}
