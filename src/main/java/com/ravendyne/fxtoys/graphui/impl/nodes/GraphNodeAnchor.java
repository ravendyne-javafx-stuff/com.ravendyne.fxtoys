package com.ravendyne.fxtoys.graphui.impl.nodes;

import com.ravendyne.constellation.graphui.api.IGraphUI.NodeType;

import javafx.scene.Node;
import javafx.scene.control.Label;
import javafx.scene.shape.Circle;

public class GraphNodeAnchor extends AbstractGraphNode {
    public static final String CSS_CLASS = "graph-box";
    public static final String CSS_CLASS_CIRCLE = "circle";

    double radius;
    
    public GraphNodeAnchor(String title) {
        super(title);
        radius = 20.0;
        buildUI();
    }
    
    public GraphNodeAnchor(String title, double radius) {
        super(title);
        this.radius = radius;
        buildUI();
    }
    
    public GraphNodeAnchor(double radius) {
        super(null);
        this.radius = radius;
        buildUI();
    }

    protected void buildUI() {
        Circle circle = new Circle();
        circle.getStyleClass().add(CSS_CLASS_CIRCLE);
        circle.setRadius(radius);
        getChildren().add(circle);

        if(title != null) {
            getChildren().add(new Label(title));
        }

        getStyleClass().add(CSS_CLASS);
    }

    @Override
    public Node getNode() {
        return this;
    }

    @Override
    public NodeType getType() {
        return NodeType.Anchor;
    }

}
