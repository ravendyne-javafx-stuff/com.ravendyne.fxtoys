package com.ravendyne.fxtoys.graphui.impl.connectors;

import com.ravendyne.constellation.geometry.Vector2d;
import com.ravendyne.constellation.graphui.api.IGraphUIConnector;
import com.ravendyne.constellation.graphui.api.IGraphUINode;

import javafx.scene.Node;
import javafx.scene.shape.Line;

public class GraphLine extends Line implements IGraphUIConnector {
    public static final String CSS_CLASS = "graph-connector-line";

    protected IGraphUINode startNode;
    protected IGraphUINode endNode;

    public GraphLine( IGraphUINode startNode, IGraphUINode endNode ) {
        getStyleClass().add(CSS_CLASS);

        this.startNode = startNode;
        this.endNode = endNode;
        
        buildUI();
    }

    private void buildUI() {
    }

    @Override
    public void setPosition( Vector2d startOnCanvas, Vector2d endOnCanvas ) {
        setStartX(startOnCanvas.x);
        setStartY(startOnCanvas.y);
        setEndX(endOnCanvas.x);
        setEndY(endOnCanvas.y);
    }

    @Override
    public void updatePosition() {
        setPosition(startNode.getPosition(), endNode.getPosition());
    }

    @Override
    public Node getNode() {
        return this;
    }

    @Override
    public IGraphUINode getStartNode() {
        return startNode;
    }

    @Override
    public IGraphUINode getEndNode() {
        return endNode;
    }
    
    @Override
    public String toString() {
        return startNode + " -> " + endNode;
    }
}
