package com.ravendyne.fxtoys.graphui.impl.nodes;

import com.ravendyne.constellation.graphui.api.IGraphUI.NodeType;

import javafx.geometry.Insets;
import javafx.scene.Node;
import javafx.scene.paint.Color;
import javafx.scene.shape.Line;

public class GraphNodeOrigin extends AbstractGraphNode {
    public static final String CSS_CLASS = "graph-origin";

    public GraphNodeOrigin(String title) {
        super(title);
        buildUI();
    }

    protected void buildUI() {
        Line vLine = new Line(0, 10, 20, 10);
        vLine.setStroke(Color.RED);
        vLine.setStrokeWidth(2.0);
        getChildren().add(vLine);
        Line hLine = new Line(10, 0, 10, 20);
        hLine.setStroke(Color.GREEN);
        hLine.setStrokeWidth(2.0);
        getChildren().add(hLine);
        getStyleClass().add(CSS_CLASS);
        setPadding(new Insets(5));
    }

    @Override
    public Node getNode() {
        return this;
    }

    @Override
    public NodeType getType() {
        return NodeType.Origin;
    }

}
