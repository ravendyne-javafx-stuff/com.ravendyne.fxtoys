package com.ravendyne.fxtoys.graphui.impl.nodes;

import com.ravendyne.constellation.geometry.Vector2d;
import com.ravendyne.constellation.graphui.api.IGraphUINode;
import com.ravendyne.fxtoys.ui.DragContextTranslationBased;

import javafx.beans.value.ChangeListener;
import javafx.beans.value.ObservableValue;
import javafx.scene.Node;
import javafx.scene.layout.StackPane;

public abstract class AbstractGraphNode extends StackPane implements IGraphUINode {

    protected String title;

    private Vector2d nodeHalfSize;
    
    public AbstractGraphNode(String title) {
        this.title = title;
        
        new DragContextTranslationBased(this);

        nodeHalfSize = Vector2d.ZERO;
        widthProperty().addListener(new ChangeListener<Number>() {
            @Override
            public void changed(ObservableValue<? extends Number> observable, Number oldValue, Number newValue) {
                nodeHalfSize = new Vector2d(newValue.doubleValue() / 2.0, nodeHalfSize.y);
            }
        });
        heightProperty().addListener(new ChangeListener<Number>() {
            @Override
            public void changed(ObservableValue<? extends Number> observable, Number oldValue, Number newValue) {
                nodeHalfSize = new Vector2d(nodeHalfSize.x, newValue.doubleValue() / 2.0);
            }
        });
    }

    /**
     * {@inheritDoc}
     * <p>
     * This implementation sets the layout position of the node.
     * Layout position is determined by {@link Node#layoutXProperty()} and {@link Node#layoutYProperty()} values.
     * </p>
     */
    @Override
    public void setPosition(Vector2d newLayoutPosition) {
        Vector2d boxTopLeftPositionOnCanvas = toNodeTopLeft( newLayoutPosition );
        relocate( boxTopLeftPositionOnCanvas.x, boxTopLeftPositionOnCanvas.y );
    }

    /**
     * {@inheritDoc}
     * <p>
     * This implementation returns layout position of the node.
     * Layout position is determined by {@link Node#layoutXProperty()} and {@link Node#layoutYProperty()} values.
     * </p> 
     */
    @Override
    public Vector2d getPosition() {
        Vector2d boxTopLeftOnCanvas = new Vector2d( getLayoutX(), getLayoutY() );
        return toNodeCenter( boxTopLeftOnCanvas );
    }

    protected Vector2d toNodeTopLeft( Vector2d nodeCenterOnCanvas ) {
        return nodeCenterOnCanvas.subtract(nodeHalfSize);
    }
    
    protected Vector2d toNodeCenter( Vector2d nodeTopLeftOnCanvas ) {
        return nodeTopLeftOnCanvas.add(nodeHalfSize);
    }

    @Override
    public String toString() {
        return "<" + getType().name() + "> '" + title + "'";
    }
}
