package com.ravendyne.fxtoys.graphui.impl;

import java.util.HashSet;
import java.util.List;
import java.util.Set;

import com.ravendyne.constellation.geometry.Vector2d;
import com.ravendyne.constellation.graphui.GraphLoader;
import com.ravendyne.constellation.graphui.api.IGraphUI;
import com.ravendyne.constellation.graphui.api.IGraphUIConnector;
import com.ravendyne.constellation.graphui.api.IGraphUINode;
import com.ravendyne.constellation.graphui.api.INodesConnectorsProvider;
import com.ravendyne.fxtoys.graphui.impl.connectors.GraphCubicCurve;
import com.ravendyne.fxtoys.graphui.impl.connectors.GraphLine;
import com.ravendyne.fxtoys.graphui.impl.connectors.GraphQuadCurve;
import com.ravendyne.fxtoys.graphui.impl.nodes.GraphNodeAnchor;
import com.ravendyne.fxtoys.graphui.impl.nodes.GraphNodeBox;
import com.ravendyne.fxtoys.graphui.impl.nodes.GraphNodeOrigin;

import javafx.beans.value.ChangeListener;
import javafx.beans.value.ObservableValue;
import javafx.scene.Node;
import javafx.scene.layout.Pane;

public class GraphPane extends Pane implements IGraphUI {
    
    public static final String CSS_CLASS = "graph-pane";
    public Set<IGraphUINode> nodes;
    public Set<IGraphUIConnector> connectors;

    public Vector2d canvasSize;
    GraphLoader loader;

    public GraphPane() {
        getStyleClass().add(CSS_CLASS);
        nodes = new HashSet<>();
        connectors = new HashSet<>();
        loader = new GraphLoader( this );

        canvasSize = Vector2d.ZERO;
        widthProperty().addListener(new ChangeListener<Number>() {
            @Override
            public void changed(ObservableValue<? extends Number> observable, Number oldValue, Number newValue) {
                canvasSize = new Vector2d(newValue.doubleValue(), canvasSize.y);
            }
        });
        heightProperty().addListener(new ChangeListener<Number>() {
            @Override
            public void changed(ObservableValue<? extends Number> observable, Number oldValue, Number newValue) {
                canvasSize = new Vector2d(canvasSize.x, newValue.doubleValue());
            }
        });
    }

    public void load(List<String> gmlSource) {
        loader.load(gmlSource);
    }

    @Override
    public IGraphUINode addNode(IGraphUINode node) {

        nodes.add(node);
        // GraphLoader will be calling us here with nodes
        // created by our newNode() method, so it's safe to cast to Node
        getChildren().add((Node) node.getNode());

        return node;
    }
    
    @Override
    public IGraphUIConnector addConnector(IGraphUIConnector connector) {
        connectors.add(connector);
        // GraphLoader will be calling us here with connectors
        // created by our newConnector() method, so it's safe to cast to Node
        getChildren().add((Node) connector.getNode());

        return connector;
    }
    
    public void update() {
        for(IGraphUIConnector connector : connectors) {
            connector.updatePosition();
        }
    }
    
    @Override
    public IGraphUIConnector newConnector(ConnectorType type, String label, IGraphUINode sourceNode, IGraphUINode targetNode) {
        IGraphUIConnector connector = null;
        
        switch(type) {
        case Line:
            connector = new GraphLine(sourceNode, targetNode);
            break;
        case Cubic:
            connector = new GraphCubicCurve(sourceNode, targetNode);
            break;
        case Quadratic:
            connector = new GraphQuadCurve(sourceNode, targetNode);
            break;
        }

        return connector;
    }
    
    @Override
    public IGraphUINode newNode(NodeType type, String label) {
        IGraphUINode node = null;
        
        switch(type) {
        case Origin:
            node = new GraphNodeOrigin(label);
            break;
        case Anchor:
            node = new GraphNodeAnchor(5.0);
            break;
        case Box:
            node = new GraphNodeBox(label);
            break;
        }

        return node;
    }

    @Override
    public Iterable< IGraphUIConnector > getConnectors() {
        return connectors;
    }

    @Override
    public Iterable< IGraphUINode > getNodes() {
        return nodes;
    }

    @Override
    public Vector2d getCanvasSize() {
        return canvasSize;
    }

    @Override
    public INodesConnectorsProvider getAllNodesConnectors() {
        return loader;
    }

}
