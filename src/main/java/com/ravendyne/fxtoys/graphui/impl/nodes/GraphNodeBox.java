package com.ravendyne.fxtoys.graphui.impl.nodes;

import com.ravendyne.constellation.graphui.api.IGraphUI.NodeType;

import javafx.scene.Node;
import javafx.scene.control.Label;
import javafx.scene.layout.StackPane;

public class GraphNodeBox extends AbstractGraphNode {
    public static final String CSS_CLASS = "graph-box";
    public static final String CSS_CLASS_PANE = "pane";

    public GraphNodeBox(String title) {
        super(title);
        buildUI();
    }

    protected void buildUI() {
        StackPane pane = new StackPane(new Label(title));
        pane.getStyleClass().add(CSS_CLASS_PANE);
        getChildren().add(pane);

        getStyleClass().add(CSS_CLASS);
    }

    @Override
    public Node getNode() {
        return this;
    }

    @Override
    public NodeType getType() {
        return NodeType.Box;
    }

}
