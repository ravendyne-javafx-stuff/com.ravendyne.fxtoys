package com.ravendyne.fxtoys.components.bezier;

import java.util.ArrayList;
import java.util.List;

import com.ravendyne.constellation.geometry.curves.IParametericCurve;
import com.ravendyne.constellation.geometry.curves.IParametericValueProducer;
import com.ravendyne.fxtoys.components.DraggablePointBox;
import com.ravendyne.fxtoys.ui.DragContextTranslationBased;

import javafx.geometry.Point2D;
import javafx.scene.canvas.Canvas;
import javafx.scene.canvas.GraphicsContext;
import javafx.scene.control.Label;
import javafx.scene.layout.Border;
import javafx.scene.layout.BorderStroke;
import javafx.scene.layout.BorderStrokeStyle;
import javafx.scene.layout.BorderWidths;
import javafx.scene.layout.CornerRadii;
import javafx.scene.layout.Pane;
import javafx.scene.paint.Color;

public class ParametricCurvePane extends Pane {
    
    // parameter step used for curve drawing
    protected final double step = 0.05;

    // the curve
    final IParametericCurve curve;
    protected IParametericValueProducer parametric;

    protected Canvas canvas;
    protected Label titleLabel;

    // canvas width and height
    protected double width;
    protected double height;
    // scaling/translation from curve space to canvas space
    protected double scale;
    protected Point2D translation;

    // UI handles for curve control points
    protected List<DraggablePointBox> handles;
    protected OriginPointBox originHandle;

    private Runnable onCurveUpdate;

    public ParametricCurvePane(IParametericCurve curve) {

        //--------------------------------------------------------------------
        //  Pane stuff
        //--------------------------------------------------------------------
        new DragContextTranslationBased( this );
        setBorder(new Border(new BorderStroke(Color.AQUAMARINE, BorderStrokeStyle.SOLID, CornerRadii.EMPTY, BorderWidths.DEFAULT)));
        setOnMousePressed((e) -> {
            toFront();
        });

        //--------------------------------------------------------------------
        //  The stuff we work for
        //--------------------------------------------------------------------
        this.curve = curve;
        parametric = curve.getValueProducer();

        //--------------------------------------------------------------------
        //  Geometry: canvas size, curve translation & scaling
        //--------------------------------------------------------------------
        this.width = 300;
        this.height = 300;
        scale = 200.0;
        translation = Point2D.ZERO;
        
        //--------------------------------------------------------------------
        //  Controls that should be below everything else (including canvas)
        //--------------------------------------------------------------------
        titleLabel = new Label();
        titleLabel.getStyleClass().add("curve-graph-title");
        titleLabel.relocate(5, 5);
        getChildren().add(titleLabel);

        //--------------------------------------------------------------------
        //  Canvas and control handles
        //--------------------------------------------------------------------
        canvas = new Canvas(width, height);
        
        initializeCanvas();

        createControlHandles();

        // add canvas AFTER handles so handles are rendered below graphics
        getChildren().add(canvas);
        // in this case, we also have to make canvas ignore mouse events
        // in order for handles to be able to capture them
        canvas.setMouseTransparent(true);

        //--------------------------------------------------------------------
        //  Zoom-in/out
        //--------------------------------------------------------------------
        setOnScroll((se) -> {
            if(se.getDeltaY() > 0) {
                // zoom in
                scale *= 0.8;
            } else {
                // zoom out
                scale /= 0.8;
            }
            refreshControlHandlesPositions();
            refreshCurveDrawing();
        });

        //--------------------------------------------------------------------
        //  Update curve on canvas
        //--------------------------------------------------------------------
        refreshCurveDrawing();
    }

    private void initializeCanvas() {
        canvas.setWidth(width);
        canvas.setHeight(height);
        clearCanvas(canvas.getGraphicsContext2D());
    }

    protected void createControlHandleOrigin() {
        // create origin handle which is independent of all others
        // and we use it to change translation of the curve (and handles)
        // on the canvas
        // we'll create this one first so it is added to pane below
        // all the other control handles, in case there's an overlap
        originHandle = new OriginPointBox();
        originHandle.setPosition( flipVertically( translation ) );
        // moving this handle, moves the entire curve
        originHandle.setOnDragStop((p) -> {
            setTranslate(flipVertically( p ));
            refreshCurveDrawing();
        });
        getChildren().add(originHandle);
    }

    protected void createControlHandlePoints() {

        for(int idx = 0; idx < curve.getControlCount(); idx++) {
            ControlPointHandlerBox handle = new ControlPointHandlerBox();
            handles.add(handle);
            
            final int ctrl = idx;

            // moving this handle, moves control point
            handle.setOnDragStop((p) -> {
                p = fromCanvasToCurveSpace(p);
                curve.setControlPosition( ctrl, p );
                refreshCurveDrawing();
            });
            
            getChildren().add(handle);
        }

        refreshControlHandlesPositions();
    }

    protected void createControlHandles() {
        handles = new ArrayList<>();

        createControlHandleOrigin();

        createControlHandlePoints();
    }

    protected Point2D fromCanvasToCurveSpace(Point2D p) {
        return flipVertically( p ).subtract( translation ).multiply( 1.0/scale );
    }

    protected Point2D fromCurveToCanvasSpace(Point2D v) {
        return flipVertically( v.multiply( scale ).add( translation ) );
    }

    protected Point2D flipVertically(Point2D positionOnCanvas) {
        return new Point2D(positionOnCanvas.getX(), -positionOnCanvas.getY()).add(new Point2D(0, height));
    }
    
    public void setOnCurveUpdate(Runnable onCurveUpdate) {
        this.onCurveUpdate = onCurveUpdate;
    }

    public void setTranslationX(double value) {
        setTranslate(new Point2D(value, 0));
    }
    
    public void setTranslationY(double value) {
        setTranslate(new Point2D(0, value));
    }

    /**
     * Bigger value zooms in, smaller value zooms out.
     * Initially, the curve size (width x height) is approximately 1.0x1.0.
     * (x,y) values are, generally, in that range and scale simply multiplies those
     * values with whatever value is given here as parameter.
     * @param value a number to multiply control point coordinates and the curve point coordinates with
     */
    public void setScale(double value) {
        this.scale = value;
        refreshControlHandlesPositions();
        refreshCurveDrawing();
    }

    protected void setTranslate(Point2D translation) {
        this.translation = translation;
        refreshControlHandlesPositions();
        refreshCurveDrawing();
    }
    
    public void setSize(double newWidth, double newHeight) {
        double scaleX = newWidth / width;
        double scaleY = newHeight / height;
        double scaling = 1.0;
        if(scaleX > 1.0 && scaleY > 1.0) {
            scaling = scaleX > scaleY ? scaleX : scaleY;
        } else {
            scaling = scaleX > scaleY ? scaleY : scaleX;
        }

        width = newWidth;
        height = newHeight;
        scale *= scaling;
        translation = translation.multiply(scaling);

        initializeCanvas();
        originHandle.setPosition( flipVertically( translation ) );
        refreshControlHandlesPositions();
        refreshCurveDrawing();
    }

    protected void refreshControlHandlesPositions() {
        for(int idx = 0; idx < handles.size(); idx++) {
            DraggablePointBox handle = handles.get(idx);
            handle.setPosition( fromCurveToCanvasSpace( curve.getControlPosition( idx ) ) );
        }
    }

    public void refreshCurveDrawing() {
        draw();
        if(onCurveUpdate != null) {
            onCurveUpdate.run();
        }
    }

    private void clearCanvas(GraphicsContext gc) {
        gc.clearRect(0, 0, width, height);
        gc.setStroke(Color.ORANGE);
        gc.strokeRect(0, 0, width, height);
    }

    private void draw() {
        GraphicsContext gc = canvas.getGraphicsContext2D();

        gc.setFill(Color.YELLOW);

        clearCanvas(gc);

        gc.setStroke(Color.CHARTREUSE);
        gc.setLineWidth(2);
        gc.beginPath();

        double t = 0.0;
        Point2D start = fromCurveToCanvasSpace(parametric.value(t));

        gc.moveTo(start.getX(), start.getY());
        gc.fillRect(start.getX() - 1, start.getY() - 1, 3, 3);

        final double tLimit = 1.0 + step/2.0;
        for (t = step; t <= tLimit; t += step) {
            Point2D bt = fromCurveToCanvasSpace(parametric.value(t));

            gc.lineTo(bt.getX(), bt.getY());
            gc.fillRect(bt.getX() - 1, bt.getY() - 1, 3, 3);
        }

        gc.stroke();
    }
    
    public void setTitle(String title) {
        titleLabel.setText(title);
    }
}
