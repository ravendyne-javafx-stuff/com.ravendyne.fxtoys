package com.ravendyne.fxtoys.components.bezier;

import com.ravendyne.fxtoys.components.DraggablePointBox;

import javafx.scene.paint.Color;

public class ControlPointHandlerBox extends DraggablePointBox {
    
    public ControlPointHandlerBox() {
        super();
    }
    
    @Override
    protected Color getColor() {
        return Color.BLUEVIOLET;
    }

}
