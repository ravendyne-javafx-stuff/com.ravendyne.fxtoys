package com.ravendyne.fxtoys.components.bezier;

import com.ravendyne.constellation.geometry.curves.bezier.BezierCurve;
import com.ravendyne.constellation.geometry.curves.bezier.BezierCurve.ViewType;
import com.ravendyne.fxtoys.components.DraggablePointBox;

import javafx.geometry.Point2D;

public class BezierPane extends ParametricCurvePane {
    final ViewType type;

    public BezierPane(BezierCurve curve, ViewType type) {
        super(curve);

        this.type = type;
        parametric = curve.forType(type).getValueProducer();

        if(type != ViewType.Curve) {
            for(DraggablePointBox handle:handles) {
                handle.setVisible( false );
            }
        } else {
            setTranslate( new Point2D(50.0, 50.0) );
            originHandle.setPosition( flipVertically( translation ) );
        }
    }
}
