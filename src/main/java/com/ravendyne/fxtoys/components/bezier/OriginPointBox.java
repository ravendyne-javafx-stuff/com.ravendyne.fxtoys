package com.ravendyne.fxtoys.components.bezier;

import com.ravendyne.fxtoys.components.DraggablePointBox;

import javafx.scene.paint.Color;

public class OriginPointBox extends DraggablePointBox {

    public OriginPointBox() {
        super();
    }
    
    @Override
    protected Color getColor() {
        return Color.GREEN;
    }

}
