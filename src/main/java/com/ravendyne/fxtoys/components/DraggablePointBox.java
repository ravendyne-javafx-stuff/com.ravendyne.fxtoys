package com.ravendyne.fxtoys.components;

import java.util.function.Consumer;

import com.ravendyne.fxtoys.ui.DragContextTranslationBased;

import javafx.geometry.Point2D;
import javafx.scene.layout.Pane;
import javafx.scene.paint.Color;
import javafx.scene.shape.Rectangle;

public class DraggablePointBox extends Pane {

    private Point2D halfSize = new Point2D(5, 5);
    final Color mouseOverColor = Color.YELLOW;
    
    private Rectangle handle;
    private Consumer<Point2D> onDragStop;
    
    public DraggablePointBox() {
        handle = new Rectangle();
        handle.setWidth(halfSize.getX() * 2);
        handle.setHeight(halfSize.getY() * 2);
        handle.setFill(getColor());
        DragContextTranslationBased drag = new DragContextTranslationBased(this);
        drag.setConsumeEvent( true );
        drag.addOnDragStopListener((p) -> onDragStop.accept(p.add(halfSize)));
        
        handle.setOnMouseEntered((e)->{
            handle.setFill(mouseOverColor);
        });
        handle.setOnMouseExited((e)->{
            handle.setFill(getColor());
        });

        getChildren().add(handle);
    }

    protected Color getColor() {
        return Color.GREEN;
    }

    public void setPosition(Point2D position) {
        setTranslateX(position.getX() - halfSize.getX());
        setTranslateY(position.getY() - halfSize.getY());
    }
    
    public Point2D getPosition() {
        return new Point2D(getTranslateX(), getTranslateY());
    }

    public void setOnDragStop(Consumer<Point2D> onDragStop) {
        this.onDragStop = onDragStop;
    }

}
