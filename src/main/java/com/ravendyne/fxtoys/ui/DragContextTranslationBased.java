package com.ravendyne.fxtoys.ui;

import javafx.geometry.Point2D;
import javafx.scene.Node;

public class DragContextTranslationBased extends DragContext {

    public DragContextTranslationBased(Node node) {
        super(node);
    }
    
    @Override
    protected void setNodePosition(Point2D position) {
        node.setTranslateX(position.getX());
        node.setTranslateY(position.getY());
    }

    @Override
    protected Point2D getNodePosition() {
        return new Point2D(node.getTranslateX(), node.getTranslateY());
    }
}
