package com.ravendyne.fxtoys.ui;

import javafx.geometry.Point2D;
import javafx.scene.Node;

public class DragContextLayoutBased extends DragContext {
    
    public DragContextLayoutBased(Node node) {
        super(node);
    }
    
    @Override
    protected void setNodePosition(Point2D position) {
        node.setLayoutX(position.getX());
        node.setLayoutY(position.getY());
    }

    @Override
    protected Point2D getNodePosition() {
        return new Point2D(node.getLayoutX(), node.getLayoutY());
    }
}
