package com.ravendyne.fxtoys.ui;

import javafx.geometry.Point2D;
import javafx.scene.Node;

public class DecoupledDragContextLayoutBased extends DecoupledDragContext {
    
    /**
     * 
     * @param dragNode node that will detect drag gesture
     * @param positionNode node that will be re-positioned on drag and
     *          will provide the current position for drag calculations
     */
    public DecoupledDragContextLayoutBased( Node dragNode, Node positionNode ) {
        super( dragNode, positionNode );
    }

    @Override
    protected void setNodePosition(Point2D position) {
        positionNode.setLayoutX(position.getX());
        positionNode.setLayoutY(position.getY());
    }

    @Override
    protected Point2D getNodePosition() {
        return new Point2D(positionNode.getLayoutX(), positionNode.getLayoutY());
    }
}
