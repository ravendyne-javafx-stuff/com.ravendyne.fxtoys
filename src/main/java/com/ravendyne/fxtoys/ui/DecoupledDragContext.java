package com.ravendyne.fxtoys.ui;

import javafx.scene.Node;


public abstract class DecoupledDragContext extends DragContext {

    protected Node positionNode;

    /**
     * 
     * @param dragNode node that will detect drag gesture
     * @param positionNode node that will be re-positioned on drag and
     *          will provide the current position for drag calculations
     */
    public DecoupledDragContext( Node dragNode, Node positionNode ) {
        super( dragNode );
        this.positionNode = positionNode;
    }

}
