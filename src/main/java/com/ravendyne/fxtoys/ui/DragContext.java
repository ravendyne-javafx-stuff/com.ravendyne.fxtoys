package com.ravendyne.fxtoys.ui;

import java.util.HashSet;
import java.util.Set;
import java.util.function.Consumer;

//import com.ravendyne.constellation.geometry.Vector2d;

import javafx.event.EventHandler;
import javafx.geometry.Point2D;
import javafx.scene.Node;
import javafx.scene.input.MouseEvent;

public abstract class DragContext implements EventHandler<MouseEvent> {
    private double mouseAnchorX;
    private double mouseAnchorY;
    private Set<Consumer<Point2D>> onDragStopListeners;
    private Set<Consumer<Point2D>> onDraggingListeners;
    private boolean consumeEvents;
    private boolean wasDragged;
    
    protected Node node;

    public DragContext(Node node) {
        this.node = node;
        this.onDragStopListeners = new HashSet<>();
        this.onDraggingListeners = new HashSet<>();
        this.wasDragged = false;

        node.addEventHandler(MouseEvent.MOUSE_PRESSED, this);
        node.addEventHandler(MouseEvent.MOUSE_DRAGGED, this);
        node.addEventHandler(MouseEvent.MOUSE_RELEASED, this);
    }
    
    public void discard() {
        node.removeEventHandler(MouseEvent.MOUSE_PRESSED, this);
        node.removeEventHandler(MouseEvent.MOUSE_DRAGGED, this);
        node.removeEventHandler(MouseEvent.MOUSE_RELEASED, this);

        onDragStopListeners.clear();
        onDraggingListeners.clear();

        onDragStopListeners = null;
        onDraggingListeners = null;
        node = null;
    }
    
    public void setConsumeEvent(boolean consume) {
        consumeEvents = consume;
    }
    
    /**
     * Adds to the list of callbacks a callback consumer to be called 
     * at the end of drag gesture, with the point where drag stopped 
     * passed as parameter to it.
     * 
     * @param onDragStopListeners
     */
    public void addOnDragStopListener(Consumer<Point2D> onDragStop) {
        onDragStopListeners.add(onDragStop);
    }
    
    public boolean removeOnDragStopListener(Consumer<Point2D> onDragStop) {
        return onDragStopListeners.remove(onDragStop);
    }
    
    /**
     * Adds to the list of callbacks a callback consumer to be called on each individual
     * drag event, with the point containing the amount of change of x and y coordinates,
     * relative to the previous drag position, passed as parameter to it.
     * 
     * @param onDraggingListeners
     */
    public void addOnDraggingListener(Consumer<Point2D> onDragging) {
        onDraggingListeners.add(onDragging);
    }
    
    public boolean removeOnDraggingListener(Consumer<Point2D> onDragging) {
        return onDraggingListeners.remove(onDragging);
    }
    
    private void start(MouseEvent event) {
        mouseAnchorX = event.getX();
        mouseAnchorY = event.getY();
    }
    
    private void drag(MouseEvent event) {
        Point2D position = getNodePosition();
        double newX = position.getX() + event.getX() - mouseAnchorX;
        double newY = position.getY() + event.getY() - mouseAnchorY;
        final Point2D newPosition = new Point2D(newX, newY);
        setNodePosition(newPosition);
        if(!onDraggingListeners.isEmpty()) {
            onDraggingListeners.forEach( (c) -> c.accept( new Point2D(event.getX() - mouseAnchorX, event.getY() - mouseAnchorY) ) );
        }
    }
    
    protected abstract void setNodePosition(Point2D position);

    protected abstract Point2D getNodePosition();

    @Override
    public void handle(MouseEvent event) {
        if(event.getEventType() == MouseEvent.MOUSE_PRESSED) {
            start(event);
        } else if(event.getEventType() == MouseEvent.MOUSE_DRAGGED) {
            wasDragged = true;
            drag(event);
        } else if(event.getEventType() == MouseEvent.MOUSE_RELEASED) {
            if(wasDragged && !onDragStopListeners.isEmpty()) {
                wasDragged = false;
                onDragStopListeners.forEach( (c) -> c.accept( getNodePosition() ) );
            }
        }
        if(consumeEvents) event.consume();
    }
}
