package com.ravendyne.fxtoys.ui;

import javafx.geometry.Point2D;
import javafx.scene.Node;

public class DecoupledDragContextTranslationBased extends DecoupledDragContext {
    
    public DecoupledDragContextTranslationBased( Node dragNode, Node positionNode ) {
        super( dragNode, positionNode );
    }

    @Override
    protected void setNodePosition(Point2D position) {
        positionNode.setTranslateX(position.getX());
        positionNode.setTranslateY(position.getY());
    }

    @Override
    protected Point2D getNodePosition() {
        return new Point2D(positionNode.getTranslateX(), positionNode.getTranslateY());
    }
}
