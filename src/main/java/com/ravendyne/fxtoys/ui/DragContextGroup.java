package com.ravendyne.fxtoys.ui;

import java.util.HashSet;
import java.util.Set;
import java.util.function.Consumer;

import javafx.geometry.Point2D;

public class DragContextGroup implements Consumer<Point2D> {
    
    Set<DragContext> contexts;
    DragContext rootDragContext;
    
    public DragContextGroup(DragContext rootDragContext) {
        contexts = new HashSet<>();
        
        this.rootDragContext = rootDragContext;
        this.rootDragContext.addOnDraggingListener(this);
    }

    public void add(DragContext dragContext) {
        if(dragContext == rootDragContext) return;

        contexts.add(dragContext);
    }

    @Override
    public void accept(Point2D delta) {
        for(DragContext context : contexts) {
            context.setNodePosition(context.getNodePosition().add(delta));
        }
    }
    
    public void discard() {
        contexts.clear();
        rootDragContext = null;
    }
}
