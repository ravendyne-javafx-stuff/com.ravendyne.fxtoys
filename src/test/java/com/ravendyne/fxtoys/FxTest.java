package com.ravendyne.fxtoys;

import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.testfx.framework.junit5.ApplicationExtension;
import org.testfx.framework.junit5.Start;

import javafx.scene.Scene;
import javafx.scene.layout.StackPane;
import javafx.stage.Stage;

@ExtendWith(ApplicationExtension.class)
public class FxTest
{
    @Start
    public void start(Stage stage) {

        StackPane stackPane = new StackPane();
        stackPane.setPrefWidth( 100 );
        stackPane.setPrefHeight( 100 );
        Scene scene = new Scene(stackPane);

        stage.setScene(scene);
        stage.setTitle("FXToys test");

        stage.show();
    }
    
    @Test
    public void gottaHaveThis() {
    }
}
